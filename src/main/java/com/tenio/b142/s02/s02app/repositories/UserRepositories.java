package com.tenio.b142.s02.s02app.repositories;

import com.tenio.b142.s02.s02app.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface UserRepositories extends CrudRepository<User, Object> {


}
